const jwt = require('express-jwt');
require('dotenv').config();

const { ACCESS_TOKEN_SECRET, API_PATH } = process.env;

exports.verifyToken = jwt(
  {
    secret: ACCESS_TOKEN_SECRET,
    algorithms: ['HS256']
  }
).unless({
  path: [
    `${API_PATH}/login`,
    `${API_PATH}/register`,
    {
      url: /\/api\/v1\/products(.*)/,
      methods: ['GET', 'OPTIONS']
    }
  ]
});

exports.verifyAdmin = (req, res, next) => {
  if (req.user.isAdmin) {
    next();
  } else {
    throw new Error('User is unauthorized');
  }
};

exports.verifySameUser = (req, res, next) => {
  if (req.user.isAdmin || req.user.id === req.params.userId) {
    next();
  } else {
    throw new Error('User is unauthorized');
  }
};
