const errorHandler = (err, req, res, next) => {

  console.log(err);

  // Duplicate Email Handler for User Registration
  if (err.code === 11000) {
    return res.status(400).send({
      error: 'Email already in use.'
    });
  }

  // Login Error Handler
  if (err.message === 'Your email or password was incorrect.') {
    return res.status(401).send({
      error: 'Your email or password was incorrect.'
    });
  }

  // User accessing admin-only or other users' routes
  if (err.message === 'User is unauthorized') {
    return res.status(401).send({
      error: 'User is unauthorized'
    });
  }

  // Handler for Redundant Admin Grant
  if (err.message === 'User is already admin') {
    return res.status(400).send({
      error: 'User is already admin'
    });
  }

  // User Not Found Error Handler
  if (err.message === 'User not found') {
    return res.status(404).send({
      error: 'User not found'
    });
  }

  // Order Not Found Error Handler
  if (err.message === 'Order not found') {
    return res.status(404).send({
      error: 'Order not found'
    });
  }

  // Order already fulfilled
  if (err.message === 'Order is already fulfilled') {
    return res.status(400).send({
      error: 'Order is already fulfilled'
    });
  }

  // Order already cancelled
  if (err.message === 'Order is already cancelled') {
    return res.status(400).send({
      error: 'Order is already cancelled'
    });
  }

  // Cart not found handler
  if (err.message === 'Cart not found') {
    return res.status(404).send({
      error: 'Cart not found'
    });
  }

  // Unavailble Product Handler
  if (err.message === 'Product not available') {
    return res.status(400).send({
      error: 'Cannot add to cart, product unavailable'
    });
  }

  // Empty Cart Error Handler
  if (err.message === 'No checked out products') {
    return res.status(400).send({
      error: 'No checked out products'
    });
  }

  // Invalid Product ID Error Handler for User Checkout
  if (err.message === 'Invalid product ID') {
    return res.status(400).send({
      error: 'Invalid product ID'
    });
  }
  // Missing Product Handler
  if (err.message === 'Product not found') {
    return res.status(404).send({
      error: 'Product not found'
    });
  }

  // Missing Cart Item Handler
  if (err.message === 'Cart item not found') {
    return res.status(404).send({
      error: 'Cart item not found'
    });
  }

  // Cast Error Handler
  if (err.name === 'CastError') {
    return res.status(400).send({
      error: 'Invalid Input'
    });
  }

  // Schema Validation Error(s) in Models
  if (err.name === 'ValidationError') {
    const messages = Object.values(err.errors).map(val => val.message);
    return res.status(400).json({
      error: messages
    });

    // Missing JWT Token
  } else if (err.message === 'No authorization token was found') {
    return res.status(401).json({
      error: 'Please login to continue.'
    });

    // Invalid JWT Token
  } else if (err.name === 'UnauthorizedError') {
    return res.status(401).json({
      error: 'Invalid token, please refresh your session'
    });

    // Expired JWT Token
  } else if (err.name === 'TokenExpiredError') {
    return res.status(401).json({
      error: 'Token expired'
    });

  } else {
    return res.status(500).json({
      error: 'Internal server error'
    });
  }
};

module.exports = errorHandler;
