const express = require('express');
const router = express.Router();
const product = require('../controllers/productController');
const review = require('../controllers/reviewController');

router.get('/', product.getActiveProducts);
router.get('/:productId/view', product.getActiveProductById);

// Get all reviews of a product
router.get('/:productId/reviews', review.getReviews);
// Create a review
router.post('/:productId/reviews', review.createReview);
// Delete a review
router.delete('/:productId/reviews/:reviewId', review.deleteReview);

// Return all possible categories
router.get('/categories', product.getCategories);

module.exports = router;
