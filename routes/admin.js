const express = require('express');
const router = express.Router();
const user = require('../controllers/userController');
const product = require('../controllers/productController');
const orderRouter = require('./orders');
const review = require('../controllers/reviewController');
const admin = require('../controllers/adminController');
const { verifyAdmin } = require('../middlewares/verifyToken');

router.use(verifyAdmin);

router.get('/dashboard', admin.getCounts);

router.get('/users', user.getAllUsers);
router.get('/users/:userId', user.getUserById);
router.patch('/users/elevate', user.elevateUser);

router.use('/orders', orderRouter);

router.get('/products', product.getAllProducts);
router.post('/products', product.createProduct);
router.patch('/products/:productId', product.updateProduct);
router.patch('/products/:productId/toggle', product.toggleProduct);

// Get all reviews
router.get('/reviews', review.getAllReviews);

module.exports = router;
