const express = require('express');
const router = express.Router();
const user = require('../controllers/userController');
const review = require('../controllers/reviewController');
const { verifySameUser } = require('../middlewares/verifyToken');

router.get('/profile', user.viewProfile);
router.get('/profile/orderhistory', user.getUserOrders);

// Get all reviews of a single user
router.get('/reviews', review.getUserReviews);
// Update a review (same user or admin only)
router.patch('/reviews/:reviewId', review.updateReview);
// Delete a review
router.delete('/reviews/:reviewId', review.deleteReview);

module.exports = router;
