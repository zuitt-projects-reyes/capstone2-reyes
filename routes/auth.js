const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config();

const User = require('../models/user');

// Registers a non-admin user
router.post('/register', async (req, res, next) => {

  const { name, email, password } = req.body;

  const user = new User({
    name,
    email,
    password
  });

  try {
    await user.save();
    res.status(201).send({
      message: 'User created successfully'
    });

  } catch (error) {
    return next(error);
  }
});

// Authenticates a user, returns a cookie containing the access token if successful
router.post('/login', async (req, res, next) => {

  const { email, password } = req.body;

  try {
    const user = await User.findOne({ email: email }).select('+password');
    if (!user) throw new Error('Your email or password was incorrect.');

    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) throw new Error('Your email or password was incorrect.');

    const accessToken = jwt.sign(
      {
        id: user._id,
        name: user.name,
        isAdmin: user.isAdmin
      },
      process.env.ACCESS_TOKEN_SECRET
    );

    res.cookie('accessToken', accessToken, {
      httpOnly: true,
      maxAge: 60 * 60 * 24 // 1 day
    });
    res.status(200).send({
      message: 'You have successfully logged in'
    });

  } catch (error) {
    return next(error);
  }
});

module.exports = router;
