const express = require('express');
const router = express.Router();
const order = require('../controllers/orderController');

router.get('/', order.getAllOrders);

router.patch('/:orderId/fulfill', order.fulfillOrder);
router.patch('/:orderId/cancel', order.cancelOrder);

module.exports = router;
