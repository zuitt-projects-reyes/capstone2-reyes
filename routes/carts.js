const express = require('express');
const router = express.Router();
const cart = require('../controllers/cartController');

router.get('/', cart.viewCart);
router.post('/add', cart.addProduct);
router.delete('/remove', cart.removeProduct);
router.delete('/clear', cart.emptyCart);

router.post('/checkout', cart.checkout);

module.exports = router;
