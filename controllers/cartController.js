const Cart = require('../models/cart');
const User = require('../models/user');
const Product = require('../models/product');
const Order = require('../models/order');

// Get cart of the signed in user
exports.viewCart = async (req, res, next) => {
  try {
    const cart = await Cart.findOne({ userId: req.user.id }).select('-userId -_id');
    if (!cart) throw new Error('Cart not found');

    res.status(200).send(cart);
  } catch (err) {
    return next(err);
  }
};


// User adds a product to cart, creates a cart if one does not exist
exports.addProduct = async (req, res, next) => {

  try {
    const { productId, quantity } = req.body;
    const product = await Product.findById({ _id: productId });
    if (!product) throw new Error('Product not found');
    if (!product.isActive) throw new Error('Product is unavailable');

    console.log(product);

    const newItem = {
      productId,
      name: product.name,
      quantity,
      price: product.price
    };

    // Create a cart if there isn't one, otherwise add to existing cart
    const cart = await Cart.findOne({ userId: req.user.id });
    if (!cart) {

      const newCart = new Cart({
        userId: req.user.id,
        items: [newItem],
        totalAmount: product.price * quantity
      });
      await newCart.save();

    } else {
      cart.add(newItem);
    }

    res.status(200).send({
      message: 'Product added to cart'
    });
  } catch (error) {
    return next(error);
  }

};

// User removes a product from the cart
exports.removeProduct = async (req, res, next) => {

  try {
    const cart = await Cart.findOne({ userId: req.user.id });
    if (!cart) throw new Error('Cart not found');

    await cart.pop(req.body.productId);

    let cartEmpty = '';
    // Drop the cart from the collection if the items array is empty
    if (cart.items.length === 0) {
      console.log(await Cart.deleteOne({ userId: req.user.id }));
      cartEmpty = ', cart is now empty';
    }

    res.status(200).json({
      message: 'Product removed from cart' + cartEmpty
    });
  } catch (error) {
    return next(error);
  }

};

// User empties the entire cart
exports.emptyCart = async (req, res, next) => {

  try {
    const cart = await Cart.findOne({ userId: req.user.id });
    if (!cart) throw new Error('Cart not found');

    await Cart.deleteOne({ userId: req.user.id });

    res.status(200).json({
      message: 'Cart emptied'
    });

  } catch (error) {
    return next(error);
  }

};

// User checks out all items in the cart and creates an order
exports.checkout = async (req, res, next) => {

  try {
    const cart = await Cart.findOne({ userId: req.user.id });
    if (!cart) throw new Error('Cart not found');

    const totalAmount = cart.getTotalPrice();

    console.log(totalAmount);

    const order = new Order({
      userId: req.user.id,
      status: 'pending',
      items: cart.items,
      totalAmount
    });

    await order.save();
    await Cart.deleteOne({ userId: req.user.id });

    res.status(200).json({
      message: 'Checkout successful, order has been placed'
    });

  } catch (error) {
    return next(error);
  }

};
