const User = require('../models/user');
const Order = require('../models/order');

// Gets all users
exports.getAllUsers = async (req, res, next) => {

  try {
    const userList = await User.find();
    res.status(200).send(userList);

  } catch (error) {
    return next(error);
  }
};

// Get a user by ID
exports.getUserById = async (req, res, next) => {
  const { userId } = req.params;

  try {
    const user = await User.findById(userId);
    if (!user) throw new Error('User not found');

    res.status(200).send(user);

  } catch (error) {
    return next(error);
  }
};

// Elevates user to admin
exports.elevateUser = async (req, res, next) => {
  const { userId } = req.body;

  try {
    const user = await User.findById(userId);
    if (!user) throw new Error('User not found');

    if (user.isAdmin) throw new Error('User is already admin');
    user.isAdmin = true;
    await user.save();
    res.status(200).send(user);

  } catch (error) {
    return next(error);
  }
};

// Get the logged in user's profile
exports.viewProfile = async (req, res, next) => {

  try {
    const user = await User.findById(req.user.id).select('-_id -isAdmin');
    if (!user) throw new Error('User not found');

    res.status(200).send(user);

  } catch (error) {
    return next(error);
  }
};

// Get all orders of the logged in user
exports.getUserOrders = async (req, res, next) => {

  const { status } = req.query;

  try {
    const user = await User.findById(req.user.id);
    if (!user) throw new Error('User not found');

    const orders = await Order.find({ userId: req.user.id }).select('-userId -updatedAt -items._id -items.productId');

    const filteredOrders = orders.filter(c => {
      return (status ? (c.status === status) : true);
    });

    res.status(200).send(filteredOrders);

  } catch (error) {
    return next(error);
  }
};
