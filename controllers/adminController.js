const User = require('../models/user');
const Product = require('../models/product');
const Order = require('../models/order');
const Review = require('../models/review');

// Display interesting data
exports.getCounts = async (req, res, next) => {

  try {

    // Count the number of admins
    const admins = await User.find({ isAdmin: true });
    const numAdmins = admins.length;
    const usersCount = await User.countDocuments();
    const productsCount = await Product.countDocuments();
    const ordersCount = await Order.countDocuments();
    const reviewsCount = await Review.countDocuments();

    // Get average rating of all products
    const products = await Product.find();
    let totalRating = 0;
    for (let i = 0; i < products.length; i++) {
      totalRating += products[i].rating;
    }
    const averageRating = totalRating / products.length;

    const orders = await Order.find({});

    // Get number of pending orders
    const pendingOrders = orders.filter(order => order.status === 'pending');


    // Get number of fulfilled orders
    const fulfilledOrders = orders.filter(order => order.status === 'fulfilled');

    const totalAmount = fulfilledOrders.reduce((acc, order) => {
      return acc + order.totalAmount;
    }, 0);

    // Get number of cancelled orders
    const cancelledOrders = orders.filter(order => order.status === 'cancelled');

    res.status(200).send({
      "Admin Count": numAdmins,
      "Current Users": usersCount,
      "Number of Products": productsCount,
      "Number of Orders": ordersCount,
      "Number of Pending Orders": pendingOrders.length,
      "Number of Fulfilled Orders": fulfilledOrders.length,
      "Number of Cancelled Orders": cancelledOrders.length,
      "Number of Reviews": reviewsCount,
      "Average Rating": averageRating,
      "Total Profit": totalAmount
    });

  } catch (error) {
    return next(error);
  }

};
