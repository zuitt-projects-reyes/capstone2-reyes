const Order = require('../models/order');
const User = require('../models/user');

// Gets all orders
exports.getAllOrders = async (req, res, next) => {

  const { status } = req.query;

  try {
    const orderList = await Order.find();

    const filteredOrders = orderList.filter(c => {
      return (status ? (c.status === status) : true);
    });

    res.status(200).send(filteredOrders);

  } catch (error) {
    return next(error);
  }
};

// Sets a single order to fulfilled
exports.fulfillOrder = async (req, res, next) => {

  try {
    const order = await Order.findById(req.params.orderId);
    if (!order) throw new Error('Order not found');
    if (order.status === 'cancelled') throw new Error('Order is already cancelled');

    order.status = 'fulfilled';

    // Update the same order in the Users collection
    await User.updateOne({
      orders: {
        $elemMatch: { _id: order._id }
      }
    }, {
      $set: { 'orders.$.status': 'fulfilled' }
    });

    await order.save();
    res.status(200).send();

  } catch (error) {
    return next(error);
  }
};

// Sets a single order to cancelled
exports.cancelOrder = async (req, res, next) => {

  try {
    const order = await Order.findById(req.params.orderId);
    if (!order) throw new Error('Order not found');
    if (order.status === 'fulfilled') throw new Error('Order is already fulfilled');

    order.status = 'cancelled';

    // Update the same order in the Users collection
    await User.updateOne({
      orders: {
        $elemMatch: { _id: order._id }
      }
    }, {
      $set: { 'orders.$.status': 'cancelled' }
    });

    await order.save();
    res.status(200).send();

  } catch (error) {
    return next(error);
  }
};
