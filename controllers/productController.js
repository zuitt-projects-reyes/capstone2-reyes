const Product = require('../models/product');

// Gets active products with limited info (for guests and users)
exports.getActiveProducts = async (req, res, next) => {

  const { category, minrating, maxrating, minprice, maxprice, numreviews } = req.query;

  try {
    const productList = await Product.find({
      isActive: true
    }).select('name description price rating categories numReviews');

    const filteredProducts = productList.filter(c => {
      return (category ? (c.categories.includes(category)) : true) &&
        (minrating ? (c.rating >= minrating) : true) &&
        (maxrating ? (c.rating <= maxrating) : true) &&
        (minprice ? (c.price >= minprice) : true) &&
        (maxprice ? (c.price <= maxprice) : true) &&
        (numreviews ? (c.numReviews >= numreviews) : true);
    });

    res.status(200).send(filteredProducts);

  } catch (error) {
    return next(error);
  }
};

// Get an active product by ID, with limited info (for guests and users)
exports.getActiveProductById = async (req, res, next) => {
  const { productId } = req.params;

  try {
    const product = await Product.findOne({
      _id: productId,
      isActive: true
    }).select('name description price rating numReviews');
    if (!product) throw new Error('Product not found');

    res.status(200).send(product);

  } catch (error) {
    return next(error);
  }
};

// Gets all products for admins
exports.getAllProducts = async (req, res, next) => {

  const { category, minrating, maxrating, minprice, maxprice, numreviews } = req.query;

  try {
    const productList = await Product.find();

    const filteredProducts = productList.filter(c => {
      return (category ? (c.categories.includes(category)) : true) &&
        (minrating ? (c.rating >= minrating) : true) &&
        (maxrating ? (c.rating <= maxrating) : true) &&
        (minprice ? (c.price >= minprice) : true) &&
        (maxprice ? (c.price <= maxprice) : true) &&
        (numreviews ? (c.numReviews >= numreviews) : true);
    });

    res.status(200).send(filteredProducts);

  } catch (error) {
    return next(error);
  }
};


// Creates a product
exports.createProduct = async (req, res, next) => {

  const { name, description, price, categories } = req.body;

  const product = new Product({
    name,
    description,
    price,
    categories
  });

  try {
    await product.save();
    res.status(201).send();

  } catch (error) {
    return next(error);
  }
};

// Updates product info
exports.updateProduct = async (req, res, next) => {
  const { productId } = req.params;

  try {
    const product = await Product.findById(productId);
    if (!product) throw new Error('Product not found');

    const updatedProduct = Object.assign(product, req.body);

    await product.save();
    res.status(200).send();

  } catch (error) {
    return next(error);
  }
};

// Toggle product availability
exports.toggleProduct = async (req, res, next) => {
  const { productId } = req.params;

  try {
    const product = await Product.findById(productId);
    if (!product) throw new Error('Product not found');
    console.log(!product.isActive);
    product.isActive = !product.isActive;
    await product.save();
    res.status(200).send();

  } catch (error) {
    return next(error);
  }
};

// Return all possible values in the categories array field, using aggregation syntax
exports.getCategories = async (req, res, next) => {
  try {

    const categories = await Product.aggregate([
      {
        '$unwind': {
          'path': '$categories',
          'includeArrayIndex': 'string',
          'preserveNullAndEmptyArrays': true
        }
      }, {
        '$group': {
          '_id': '$categories'
        }
      }
    ]);

    const uniqueCategories = [...new Set(categories.map(c => c._id))];

    // sort uniqueCategories alphabetically
    uniqueCategories.sort();
    res.status(200).send(uniqueCategories);

  } catch (error) {
    return next(error);
  }
};