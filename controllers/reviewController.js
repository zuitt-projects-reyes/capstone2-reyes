const Review = require('../models/review');
const Product = require('../models/product');
const User = require('../models/user');

// Given the request parameter of productId, create a review from the current user
// and the product with the given productId
exports.createReview = async (req, res, next) => {
  try {

    const product = await Product.findById(req.params.productId);
    if (!product) return next(new Error('Product not found'));

    const user = await User.findById(req.user.id);
    if (!user) return next(new Error('User not found'));

    const review = new Review({
      productId: req.params.productId,
      userId: req.user.id,
      name: req.user.name,
      rating: req.body.rating,
      content: req.body.content
    });

    await review.save();

    // When a review is saved, update the product's numReviews and averageRating using async await
    product.numReviews++;
    // Get the average rating of all reviews of the product
    const reviews = await Review.find({ productId: req.params.productId });
    let sum = 0;
    for (let i = 0; i < reviews.length; i++) {
      sum += reviews[i].rating;
    }
    product.rating = sum / reviews.length;

    // round product rating to 1 decimal place
    product.rating = Math.round(product.rating * 10) / 10;


    await product.save();
    res.status(200).send({
      message: 'Review created successfully'
    });

  } catch (error) {
    return next(error);
  }
};

// Get all reviews of a product (for guests)
exports.getReviews = async (req, res, next) => {

  const { minrating, maxrating } = req.query;

  try {
    // Check if product exists
    const product = await Product.findById(req.params.productId);
    if (!product) return next(new Error('Product not found'));

    // Get all reviews of the product
    const reviews = await Review.find({ productId: req.params.productId }).select('-_id name rating content createdAt');

    // Filter reviews by rating if minrating and maxrating are given
    const filteredReviews = reviews.filter(c => {
      return (minrating ? (c.rating >= minrating) : true) &&
        (maxrating ? (c.rating <= maxrating) : true);
    });

    res.status(200).send(filteredReviews);

  } catch (error) {
    return next(error);
  }
};

// Get all reviews of a single user
exports.getUserReviews = async (req, res, next) => {

  const { productid, minrating, maxrating } = req.query;

  try {
    // Check if user exists
    const user = await User.findById(req.user.id);
    if (!user) return next(new Error('User not found'));

    // If productId is specified, check if product exists
    if (productid) {
      const product = await Product.findById(productid);
      if (!product) return next(new Error('Product not found'));
    }

    // Get all reviews of the user
    const reviews = await Review.find({ userId: req.user.id });

    // Filter reviews by productId if productId is given
    const filteredReviews = reviews.filter(c => {
      return (productid ? (String(c.productId) === productid) : true) &&
        (minrating ? (c.rating >= minrating) : true) &&
        (maxrating ? (c.rating <= maxrating) : true);
    });

    res.status(200).send(reviews);

  } catch (error) {
    return next(error);
  }
};

// Get all reviews (for admin)
exports.getAllReviews = async (req, res, next) => {

  const { productid, minrating, maxrating } = req.query;

  try {
    // Get all reviews
    const reviews = await Review.find();

    // If productId is specified, check if product exists
    if (productid) {
      const product = await Product.findById(productid);
      if (!product) return next(new Error('Product not found'));
    }

    // Filter reviews by productId if productId is given
    const filteredReviews = reviews.filter(c => {
      return (productid ? (String(c.productId) === productid) : true) &&
        (minrating ? (c.rating >= minrating) : true) &&
        (maxrating ? (c.rating <= maxrating) : true);
    });

    res.status(200).send(filteredReviews);

  } catch (error) {
    return next(error);
  }

};

// Update a review (same user or admin only)
exports.updateReview = async (req, res, next) => {

  try {
    // Check if review exists
    const review = await Review.findById(req.params.reviewId);
    if (!review) return next(new Error('Review not found'));

    // Update review
    review.rating = req.body.rating;
    review.content = req.body.content;

    await review.save();

    res.status(200).send({
      message: 'Review updated successfully'
    });

  } catch (error) {
    return next(error);
  }
};

// Delete a review (same user or admin only)
exports.deleteReview = async (req, res, next) => {

  try {
    // Check if review exists
    const review = await Review.findById(req.params.reviewId);
    if (!review) return next(new Error('Review not found'));

    // Check if review belongs to user or admin
    if (review.userId !== req.user.id && !req.user.isAdmin) {
      throw new Error('You are not authorized to delete this review');
    }

    // Delete the review
    await review.remove();

    // When a review is deleted, update the product's numReviews and averageRating using async await
    const product = await Product.findById(review.productId);
    product.numReviews--;
    // Get the average rating of all reviews of the product
    const reviews = await Review.find({ productId: review.productId });
    let sum = 0;
    for (let i = 0; i < reviews.length; i++) {
      sum += reviews[i].rating;
    }
    product.rating = sum / reviews.length;

    res.status(200).send({
      message: 'Review deleted successfully'
    });

  } catch (error) {
    return next(error);
  }
};
