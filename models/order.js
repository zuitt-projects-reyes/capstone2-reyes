const mongoose = require('mongoose');
const { productSchema, Product } = require('../models/product');

// TODO add error messages to enum and required
const orderSchema = mongoose.Schema({

  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },

  status: {
    type: String,
    enum: ['pending', 'fulfilled', 'cancelled'],
    default: 'pending'
  },

  items: [{
    // TODO need to add some sort of validation before populating items
    productId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Product',
      required: true
    },

    // Will be copied from product reference
    name: {
      type: String,
      required: true
    },

    // Quantity must be a positive integer
    quantity: {
      type: Number,
      required: true,
      min: [1, 'Quantity must be greater than 0'],
      validate: {
        validator: val => val % 1 === 0,
        message: 'Input quantity is not an integer'
      }
    },

    // Will be copied from product reference
    price: {
      type: Number,
      required: true
    }
  }],

  totalAmount: {
    type: Number,
    required: true
  },

  purchasedAt: {
    type: Date,
    immutable: true,
    default: () => Date.now()
  },

  updatedAt: {
    type: Date,
    default: () => Date.now()
  }
}, {
  versionKey: false
});

// Exporting both schema and model is necessary if the schema needs to be embedded
// in a one-to-one or one-to-few relationship, while having a copy in another collection
module.exports = mongoose.model('Order', orderSchema);
