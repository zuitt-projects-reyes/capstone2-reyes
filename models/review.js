const mongoose = require('mongoose');

const reviewSchema = mongoose.Schema({

  productId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Product',
    required: true
  },

  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },

  name: {
    type: String,
    required: true
  },

  rating: {
    type: Number,
    min: 1,
    max: 5,
    required: true,
    validate: {
      validator: v => v % 1 === 0,
      message: 'Rating must be an integer'
    }
  },

  content: {
    type: String,
    required: true,
    minlength: [3, 'Review must be at least 3 characters long']
  }
}, {
  timestamps: true,
  versionKey: false
});

module.exports = mongoose.model('Review', reviewSchema);
