const mongoose = require('mongoose');

const productSchema = mongoose.Schema({

  name: {
    type: String,
    required: true
  },

  description: {
    type: String,
    required: true
  },
  // Remember to round to one decimal place
  rating: {
    type: Number,
    min: 0,
    default: 0,
  },

  numReviews: {
    type: Number,
    min: 0,
    default: 0
  },

  price: {
    type: Number,
    required: true,
    min: 0
  },

  categories: {
    type: [String],
    default: []
  },

  isActive: {
    type: Boolean,
    default: true
  }
}, {
  timestamps: true,
  versionKey: false
});

module.exports = mongoose.model('Product', productSchema);
