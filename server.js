const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();

const app = express();
const { verifyToken } = require('./middlewares/verifyToken');
const authRouter = require('./routes/auth');
const userRouter = require('./routes/users');
const productRouter = require('./routes/products');
const cartRouter = require('./routes/carts');
const adminRouter = require('./routes/admin');

const errorHandler = require('./middlewares/errorHandler');

// Import environmental variables from .env file
const { API_PATH, DB_CONNECTION } = process.env;
const PORT = process.env.PORT || 9001;

// Connect to database
mongoose.connect(DB_CONNECTION, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

// Middleware
app.use(cors());
app.options('*', cors());
app.use(express.json());
app.use(verifyToken);

// Routes
app.use(`${API_PATH}/`, authRouter);
app.use(`${API_PATH}/users`, userRouter);
app.use(`${API_PATH}/products`, productRouter);
app.use(`${API_PATH}/cart`, cartRouter);
app.use(`${API_PATH}/admin`, adminRouter);

app.use(errorHandler);

// Start the server
app.listen(PORT, () => {
  console.log(`Listening on http://localhost:${PORT}` + API_PATH);
});
